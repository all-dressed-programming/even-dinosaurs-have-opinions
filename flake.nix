{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nix-deno.url = "github:nekowinston/nix-deno";
  };

  outputs = { self, nixpkgs, flake-utils, nix-deno }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ nix-deno.overlays.default ];
        };

        frontend = pkgs.denoPlatform.mkDenoDerivation {
           name = "frontend";
           src = ./.;
           buildPhase = ''
            deno run -A src/cli.ts bundle-comments comments-wui.json
           '';
           installPhase = ''
               mkdir $out
               cp comments-wui.json  $out/comments-wui.json
           '';
       };

        public-files = pkgs.stdenv.mkDerivation {
                    name = "public-files";
                    src = ./.;
                    buildPhase = "";
                    installPhase = ''
                      mkdir $out
                      cp -r * $out
                    '';
                };

        deploy = pkgs.writeShellApplication {
                    name = "deploy";
                    runtimeInputs = [ pkgs.deno ];
                    text = ''
                      config_dir=$(mktemp -d --suffix all-dressed-programming-deploy)
                      echo "Will use $config_dir as config directory"
                      cd ${public-files}
                      ${pkgs.deno}/bin/deno run \
                          -A https://deno.land/x/deploy/deployctl.ts deploy \
                          --save-config \
                          --config "$config_dir"/deno.json \
                          --project=even-dinosaurs \
                          ${public-files}/src/serverapp.ts
                  '';
                };
        deploy-prod = pkgs.writeShellApplication {
            name = "deploy";
            runtimeInputs = [ pkgs.deno ];
            text = ''
              config_dir=$(mktemp -d --suffix all-dressed-programming-deploy)
              echo "Will use $config_dir as config directory"
              cd ${public-files}
              ${pkgs.deno}/bin/deno run \
                  -A https://deno.land/x/deploy/deployctl.ts  deploy \
                  --prod \
                  --save-config \
                  --config "$config_dir"/deno.json \
                  --project=even-dinosaurs \
                  ${public-files}/src/serverapp.ts
          '';
        };

        test = pkgs.denoPlatform.mkDenoDerivation {
            name = "test";
            src = ./.;
            buildPhase = ''
            deno test --reporter=junit --unstable-kv  src/comments/server/handlers_test.ts > junit.xml
            '';
            installPhase = ''
                mkdir $out
                cp junit.xml  $out/junit.xml
            '';
        };
      in 
        {
          packages = {
            test = test;
            deploy = deploy;
            public-files = public-files;
            frontend = frontend;
            deploy-prod = deploy-prod;
          };
          devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [nodejs_20
                                      deno
                                      esbuild
                                      ];
          };
        }
    );
}
