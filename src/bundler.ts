import * as esbuild from 'https://deno.land/x/esbuild@v0.23.1/wasm.js';
import {denoPlugins} from '@luca/esbuild-deno-loader';
import * as path from '@std/path';

let esbuildInitialized = false;

async function verifyEsbuildInitialized(): Promise<void> {
    if (esbuildInitialized) {
        return;
    }
    await esbuild.initialize({});
    esbuildInitialized = true;
}

export type JSFile = {
    hash: string;
    content: string;
    name: string;
};

export type Bundle = {
    source?: JSFile;
    sourcemap?: JSFile;
};

type FileOutput = {
    path: string;
    hash: string;
    text: string;
};

function createBundleFromFileOutput(fileOutput: FileOutput): JSFile {
    return {
        hash: fileOutput.hash,
        content: fileOutput.text,
        name: path.basename(fileOutput.path),
    };
}

function createBundleFromFileOutputs(outputFiles: FileOutput[]): Bundle {
    const sourcemap = outputFiles.filter((x) => x.path.endsWith('.map')).map(createBundleFromFileOutput)[0];
    const source = outputFiles.filter((x) => x.path.endsWith('.js')).map(createBundleFromFileOutput)[0];
    return { sourcemap, source };
}

export async function createBundle(
    entryPoint: URL,
    importMapURL: URL,
    isDev?: boolean,
): Promise<Bundle> {
    const plugins = denoPlugins({ importMapURL: importMapURL.href });
    const res = await esbuild.build({
        bundle: true,
        entryPoints: [entryPoint.href],
        platform: 'neutral',
        treeShaking: true,
        minify: !isDev,
        sourcemap: true,
        outfile: '',
        absWorkingDir: Deno.env.get('XDG_CACHE_HOME'),
        outdir: '.',
        target: ['chrome99', 'firefox99', 'safari15'],
        write: false,
        format: 'esm',
        plugins: plugins,
        splitting: false,
        jsx: 'automatic',
        jsxImportSource: 'react',
    });
    if (res.errors.length > 0) {
        throw res.errors;
    }
    await esbuild.stop();
    return createBundleFromFileOutputs(res.outputFiles as unknown as FileOutput[]);
}

export async function getPrecompileBundle(file: string): Promise<Bundle | null> {
    try {
        return JSON.parse(await Deno.readTextFile(file)) as Bundle;
    } catch (_e) {
        return null;
    }
}
