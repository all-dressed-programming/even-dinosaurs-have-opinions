import {createBundle} from "./bundler.ts";
import { Command } from "https://deno.land/x/cliffy@v1.0.0-rc.4/command/mod.ts";
import * as path from "@std/path";


const bundleComments = new Command()
    .arguments("<destination:string>")
    .description("Clone a repository into a newly created directory.")
    .action(async (options: any, destination: string) => {
        await Deno.mkdir(path.dirname(destination), { recursive: true });
        const prodClientBundle = await createBundle(
            new URL("./comments/frontend/wui.ts", import.meta.url),
            new URL("../deno.json", import.meta.url),
            false,
        );
        return Deno.writeTextFile(
            destination,
            JSON.stringify(prodClientBundle)
        )
    });

await new Command()
.command("bundle-comments", bundleComments)
.parse(Deno.args);

