import m from '@mithril';
import { CVnode, Vnode } from '@mithril';
import { Comment, CommentPermission, CommentService } from '../shared/commentService.ts';
import { ApplicationState } from './state.ts';

interface CommentComponentAtts {
	state: ApplicationState;
	commentService: CommentService;
	comment: Comment;
}

export class CommentComponent implements m.ClassComponent<CommentComponentAtts> {
	private readonly state: ApplicationState;
	private readonly commentService: CommentService;
	private comment: Comment;

	constructor({ attrs }: CVnode<CommentComponentAtts>) {
		this.state = attrs.state;
		this.commentService = attrs.commentService;
		this.comment = attrs.comment;
	}

	voteUpCallback = () => {
		if (this.comment.hasVotedDown) {
			return;
		}
		if (this.comment.hasVotedUp) {
			this.cancelVoteUp();
			return;
		}
		this.voteUp();
	};

	voteUp = () => {
		this.commentService.voteUp(this.comment.id, this.state.secret);
		this.comment.numberOfUpVote += 1;
		this.comment.hasVotedUp = true;
	};

	cancelVoteUp = () => {
		this.commentService.cancelVoteUp(this.comment.id, this.state.secret);
		this.comment.numberOfUpVote -= 1;
		this.comment.hasVotedUp = false;
	};

	voteDownCallback = () => {
		if (this.comment.hasVotedUp) {
			return;
		}
		if (this.comment.hasVotedDown) {
			this.cancelVoteDown();
			return;
		}
		this.voteDown();
	};

	trashComment = () => {
		this.commentService.trash(this.comment.id, this.state.secret)
			.then(() => this.commentService.getComments(this.state.secret))
			.then(m.redraw);
	};

	voteDown = () => {
		this.commentService.voteDown(this.comment.id, this.state.secret);
		this.comment.numberOfDownVote += 1;
		this.comment.hasVotedDown = true;
	};

	cancelVoteDown = () => {
		this.commentService.cancelVoteDown(this.comment.id, this.state.secret);
		this.comment.numberOfDownVote -= 1;
		this.comment.hasVotedDown = false;
	};

	renderCommentActions(comment: Comment, secret: string): Vnode<any, any> {
		const style = 'margin-top: 1em;';
		const selectedStyle = '';
		const notSelectedStyle = 'color: #f5f5f5';
		return m(
			'div',
			{ style },
			m('span', [
				[
					m('i.fa-solid.fa-thumbs-up', {
						onclick: this.voteUpCallback,
						style: this.comment.hasVotedUp ? selectedStyle : notSelectedStyle,
					}),
					` × ${comment.numberOfUpVote} `,
				],
				[
					m('i.fa-solid.fa-thumbs-down', {
						onclick: this.voteDownCallback,
						style: this.comment.hasVotedDown ? selectedStyle : notSelectedStyle,
					}),
					` × ${comment.numberOfDownVote} `,
				],
				comment.permission == CommentPermission.Editor
					? m('i.fa-solid.fa-trash', { onclick: this.trashComment })
					: null,
			]),
		);
	}

	renderCommentInfo(comment: Comment): Vnode<any, any> {
		const style = 'margin-top: 1em;';
		const month = comment.date.getMonth() + 1;
		const date = comment.date.getDate();
		const text = `${comment.author} – ${comment.date.getFullYear()}-${month < 10 ? '0' + month : month}-${
			month < 10 ? '0' + date : date
		}`;
		return m('div', { style }, m('span', text));
	}

	renderCommentText(comment: Comment): Vnode<any, any> {
		const style = 'font-family: monospace;';
		return m('div', { style }, comment.comment);
	}

	view(attr) {
		const style = 'border-left: solid; margin-bottom: 2em; padding-left: 3px;';
		return m('div', { style }, [
			this.renderCommentText(this.comment),
			this.renderCommentInfo(this.comment),
			this.renderCommentActions(this.comment, this.state.secret),
		]);
	}
}
