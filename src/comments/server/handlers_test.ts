import {assertEquals, assertExists, assertFalse, assert} from "jsr:@std/assert";

import {
	CommentStorageService,
	DenoKVStorageCommentStorageService,
	LocalStorageCommentStorageService,
	PostgresCommentStorageService
} from './handlers.ts';
import {Pool} from "@dewars/postgres/";


type CommentStorageServiceFactory = () => Promise<CommentStorageService>

async function commentStorageServiceTestSuite(serviceFactory: CommentStorageServiceFactory, t: Deno.TestContext): Promise<void>{

	const payload = {
		author: 'sir arthur conan doyle',
		comment: 'sherlock.. hmmm... interresting name..',
		authorSecret: '221b baker street',
	};

	await t.step('basic create comment', async () => {

		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'basic create comment' };
		assertEquals([], await service.listComment(location));
		const newComment = await service.createComment(location, payload);
		assertEquals([newComment], await service.listComment(location));
	});

	await t.step('basic get comment', async () => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'basic get comment' };
		assertEquals([], await service.listComment(location));
		const newComment = await service.createComment(location, payload);
		assertEquals(await service.getComment(location, newComment.id), newComment);
	});

	await t.step('basic votes up', async () => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'basic votes up' };
		const newComment = await service.createComment(location, payload);
		assertEquals(newComment.votes.up, []);
		const ret = await service.voteUp(location, newComment.id, 'aaaa');
		assertExists(ret);
		assertEquals(ret.votes.up.length, 1);

		const ret2 = await service.getComment(location, newComment.id);
		assertExists(ret2);
		assertEquals(ret2.votes.up.length, 1);
	});



	await t.step('cannot vote up twice', async () => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'cannot vote up twice' };
		const newComment = await service.createComment(location, payload);
		assertEquals(newComment.votes.up, []);
		await service.voteUp(location, newComment.id, 'aaaa');
		await service.voteUp(location, newComment.id, 'aaaa');

		const ret = await service.getComment(location, newComment.id);
		assertExists(ret);
		assertEquals(ret.votes.up.length, 1);
	});


	await t.step('cancel up vote', async () => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'cancel up vote' };
		const newComment = await service.createComment(location, payload);
		const ret = await service.voteUp(location, newComment.id, 'aaaa');
		assertExists(ret);
		assertEquals(ret.votes.up.length, 1);

		await service.cancelVoteUp(location, newComment.id, 'aaaa');

		const ret2 = await service.getComment(location, newComment.id);
		assertExists(ret2);
		assertEquals(ret2.votes.up.length, 0);
	});

	await t.step('cannot cancel other vote', async () => {
		const service = await serviceFactory();

		const location = {
			domain: 'example.com',
			pageId: 'cannot cancel other vote',
		};
		const newComment = await service.createComment(location, payload);
		const ret = await service.voteUp(location, newComment.id, 'aaaa');
		assertExists(ret);
		assertEquals(ret.votes.up.length, 1);

		await service.cancelVoteUp(location, newComment.id, 'aaab');

		const ret2 = await service.getComment(location, newComment.id);
		assertExists(ret2);
		assertEquals(ret2.votes.up.length, 1);
	});

	await t.step('delete comments', async () => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'delete comments' };
		await service.createComment(location, payload);
		const newComment = await service.createComment(location, payload);
		await service.createComment(location, payload);
		const ret = await service.deleteComment(location, newComment.id, payload.authorSecret);
		assert(ret)
		assertEquals((await service.listComment(location)).length, 2);
	});

	await t.step('delete comment that does not exists', async (): Promise<void> => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'delete comments' };
		await service.createComment(location, payload);
		await service.createComment(location, payload);
		const ret = await service.deleteComment(location, 'abc', 'defg');
		assertFalse(ret);
		assertEquals((await service.listComment(location)).length, 2);
	});

	await t.step('delete comments with wrong secret', async () => {
		const service = await serviceFactory();

		const location = { domain: 'example.com', pageId: 'delete comments' };
		const newComment = await service.createComment(location, payload);
		const ret = await service.deleteComment(location, newComment.id, 'fake-secret');
		assertFalse(ret)
		assertEquals((await service.listComment(location)).length, 1);
	});

}

Deno.test('Test Local Storage', async (t: Deno.TestContext) => {
	const serviceFactory = () => {
		localStorage.clear();
		return Promise.resolve(new LocalStorageCommentStorageService());
	}

	await commentStorageServiceTestSuite(serviceFactory, t)
	localStorage.clear();
});

function hasPostgresAccess(): boolean {
	const perm = Deno.permissions.querySync(
		{ name: 'env', variable: 'POSTGRES_URL' } as const,
	);
	return perm.state === 'granted' && !!Deno.env.get('POSTGRES_URL');
}

function hasDenoKVAccess(): boolean {
	return Deno.hasOwnProperty('openKv');
}

Deno.test({
	name: 'Test Postgres',
	ignore: !hasPostgresAccess(),
	fn: async (t) => {
		const url = Deno.env.get('POSTGRES_URL');

		const cleanDB = async (pool: Pool) => {
			const client = await pool.connect();
			try {
				await client.queryArray('DELETE FROM comment_votes');
				await client.queryArray('DELETE FROM comments');
			} finally {
				client.release();
			}
		};


		async function commentStorageServiceFactory(): Promise<CommentStorageService>{

			await cleanDB(pool);
			return new PostgresCommentStorageService(pool);
		}

		const pool = new Pool(url, 3, true);
		await commentStorageServiceTestSuite(commentStorageServiceFactory, t);

	},
});


Deno.test( {
	name: 'Test deno KV',
	ignore: !hasDenoKVAccess(),
	fn: async (t) => {

		const kv = await Deno.openKv();

		async function cleanKv(kv: Deno.Kv): Promise<void>{
			for await (const entry of kv.list({ prefix: []})){
				await kv.delete(entry.key)
			}
		}

		async function serviceFactory(){
			await cleanKv(kv);
			return new DenoKVStorageCommentStorageService(kv);
		}

		await commentStorageServiceTestSuite(serviceFactory, t)
		kv.close()
	},
} );