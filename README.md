# Even Dinosaurs Have Opinions

![ss](images/logo.png)

A lightweight comment platform built to be self hosted on [deno.deploy][1] with
a [postgres][2] backend.

## How to use

### Requirements

### Frontend

### Backend

## Development

### Architecture

[1]: https://deno.com/deploy
[2]: https://www.postgresql.org/


#### Deno KV indexes

| name              | keys                                                     |            content            |
|:------------------|:---------------------------------------------------------|:-----------------------------:|
| KvCommentKey      | `['v1', 'comments', domain, pageId, lastIndexId]`        | `{content: "", metadata: ""}` |
| KvCommentKeyIndex | `['v1', 'increasing-index', 'comments', domain, pageId]` |             `45`              |