import { CommentComponent } from './comment.ts';
import { NewComment } from './newcomment.ts';
import { CommentService, RemoteStorageCommentService } from '../shared/commentService.ts';
import { getBrowserFingerprint } from './fingerprint.ts';
import { ApplicationState } from './state.ts';

import m from '@mithril';

class CommentsComponent {
	state: ApplicationState;
	commentService: CommentService;

	constructor(node) {
		this.commentService = node.attrs.commentService;
		this.state = node.attrs.state;
	}

	view() {
		return m(
			'div',
			this.state.comments.map((comment) =>
				m(CommentComponent, {
					key: comment.id,
					state: this.state,
					commentService: this.commentService,
					comment,
				})
			),
		);
	}
}

interface MainComponentAtts {
	commentsId: string;
	apiBaseURL: string;
}

class MainComponent implements m.ClassComponent<MainComponentAtts> {
	commentService: CommentService;
	state: ApplicationState;
	commentsId: string;
	apiBaseURL: string;

	constructor({ attrs }: m.CVnode<MainComponentAtts>) {
		this.commentsId = attrs.commentsId;
		this.apiBaseURL = attrs.apiBaseURL;
		this.commentService = new RemoteStorageCommentService(
			attrs.commentsId,
			attrs.apiBaseURL,
		);
	}

	oninit(): void {
		getBrowserFingerprint()
			.then((fp) => this.state = new ApplicationState([], fp))
			.then(() => this.commentService.getComments(this.state.secret))
			.then((comments) => this.state.comments = comments)
			.then(m.redraw);
	}

	isStateLoaded(): boolean {
		return !!this.commentService && !!this.state;
	}

	emptyState() {
		return m('div');
	}

	view() {
		if (!this.isStateLoaded()) {
			return this.emptyState();
		}
		return m('div', [
			m(CommentsComponent, {
				commentService: this.commentService,
				state: this.state,
			}),
			m(NewComment, { commentService: this.commentService, state: this.state }),
		]);
	}
}

export function main() {
	const elm = window.document.getElementById('even-dinosaurs-have-opinions');
	if(!elm){
		console.log("Does not have any div with id even-dinosaurs-have-opinions to use as placeholder")
		return;
	}
	const commentsId = elm.getAttribute('data-page-id') ||
		new URL(window.document.location.href).pathname;
	const apiBaseURL = elm.getAttribute('data-api-base-url') ||
		new URL(window.document.location.href).pathname;
	m.mount(elm, {
		view: () => m(MainComponent, { commentsId, apiBaseURL }),
	});
}

main();
