import m from '@mithril';
import {CommentService} from '../shared/commentService.ts';
import {ApplicationState} from './state.ts';

interface NewCommentAttrs {
	state: ApplicationState;
	commentService: CommentService;
}

export class NewComment implements m.ClassComponent<NewCommentAttrs> {
	private readonly state: ApplicationState;
	private readonly commentService: CommentService;
	private comment: string;
	private name: string;
	private isSubmiting = false;

	constructor({ attrs }: m.CVnode<NewCommentAttrs>) {
		this.state = attrs.state;
		this.commentService = attrs.commentService;
		this.comment = '';
		this.name = '';
	}

	setComment = (v) => {
		this.comment = v.target.value;
	};

	setName = (v) => {
		this.name = v.target.value;
	};

	canSubmit(): boolean {
		return !this.isSubmiting && !!this.name && !!this.comment;
	}

	async postComment(): Promise<boolean> {
		const newComment = await this.commentService.postComment(
			this.comment,
			this.name,
			this.state.secret,
		);
		this.state.comments = [newComment, ...this.state.comments];
		return true;
	}

	resetValuesAfterSubmit = (isError: boolean) => {
		this.isSubmiting = false;
		this.comment = '';
		m.redraw();
	};

	onsubmit = () => {
		if (!this.canSubmit()) {
			return;
		}
		this.isSubmiting = true;
		this.postComment()
			.then(() => this.resetValuesAfterSubmit(false))
			.catch(() => this.resetValuesAfterSubmit(true));
	};

	public view(attr) {
		const style = 'width: 100%; resize: none; font-family: monospace;';
		return m(
			'div',
			m('textarea', {
				style,
				rows: 8,
				value: this.comment,
				oninput: this.setComment,
				placeholder: 'Me too I have something to say!',
			}),
			m(
				'div',
				m(
					'label',
					'Name ',
					m('input', {
						type: 'text',
						placeholder: 'Steve From Laval',
						oninput: this.setName,
					}),
				),
			),
			m(
				'div',
				{ style: 'margin-top: 0.5em;' },
				m(
					'button',
					{ disabled: !this.canSubmit(), onclick: this.onsubmit },
					'Comment',
				),
			),
		);
	}
}
