import {Application} from "jsr:@oak/oak/application";
import {Router} from "jsr:@oak/oak/router";
import {buildCommentAPI, DenoKVStorageCommentStorageService} from './comments/server/handlers.ts';
import {oakCors} from "@tajpouria/cors/";
import {Bundle, createBundle, getPrecompileBundle} from "./bundler.ts";
import {createCommentDemoEndpoints} from "./comments/demo.ts";

async function getCommentsUIFiles(): Promise<Bundle> {
    const precompiledBundle = await getPrecompileBundle("static/comments-wui.json");
    if (precompiledBundle) {
        return precompiledBundle;
    }
    console.log("compiling comment frontend code..")
    return createBundle(
        new URL('./comments/frontend/wui.ts', import.meta.url),
        new URL('../deno.json', import.meta.url),
        false,
    );
}

const app = new Application();
const baseRouter = new Router();

baseRouter.use(
    '/comments',
    buildCommentAPI(new Router(), new DenoKVStorageCommentStorageService(await Deno.openKv()), getCommentsUIFiles()).routes(),
);
baseRouter.use(
    '/comments-demo',
    createCommentDemoEndpoints(new Router()).routes(),
);

app.use(oakCors());
app.use(baseRouter.routes());
await app.listen({ port: 8001 });
