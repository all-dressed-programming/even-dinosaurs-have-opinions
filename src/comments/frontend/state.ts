import m from '@mithril';
import { Comment } from '../shared/commentService.ts';


export class ApplicationState {
	private _comments: Comment[] = [];
	private _secret: string = window.crypto.randomUUID();

	constructor(comments: Comment[], secret: string) {
		this._comments = comments;
		this._secret = secret;
	}

	public get comments(): Comment[] {
		return [...this._comments];
	}

	public set comments(comments: Comment[]) {
		this._comments = [...comments];
		m.redraw();
	}

	public get secret(): string {
		return this._secret;
	}

	public set secret(secret: string) {
		this._secret = secret;
		m.redraw();
	}
}
