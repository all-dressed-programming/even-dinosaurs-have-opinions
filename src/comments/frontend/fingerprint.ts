function rot13(x: string): string {
	const chars = [];
	const a = 'a'.charCodeAt(0);
	for (let i = 0; i < x.length; i++) {
		chars.push(a + (((x.charCodeAt(i) - a) + 13) % 26));
	}
	return String.fromCharCode(...chars);
}

const fingerprintKey = rot13('fingerprint');

function fingerprintInLocalStorage(): string {
	return (window.localStorage.getItem(fingerprintKey) || '');
}

function isFingerPrintInLocalStorage(): boolean {
	return fingerprintInLocalStorage().length == 44;
}

async function hash(data: string): Promise<string>{
	const canvasData = (new TextEncoder()).encode(data);
	const canvasFingerprint = await crypto.subtle.digest('sha-256', canvasData);
	return String.fromCharCode(...new Uint8Array(canvasFingerprint));
}

async function canvasFingerprint(): Promise<string> {
	if (isFingerPrintInLocalStorage()) {
		return Promise.resolve(fingerprintInLocalStorage());
	}

	let canvasElement = document.createElement('canvas');
	let canvasContext = canvasElement.getContext('2d');
	canvasContext.fillStyle = 'rgba(0,0,0,0.5)';

	canvasContext.fillText('Hello mon coco ☀️ 🙋🏽', 0, 100);

	canvasContext.fillText('d\ne\tdkd', 50, 50);
	canvasContext.beginPath();
	canvasContext.arc(20, 20, 20, 0, 6);
	canvasContext.fillStyle = 'red';
	canvasContext.fill();
	const ret = await hash(canvasElement.toDataURL())
	return btoa(ret);
}

function jsFingerprint(): Promise<string> {
	const knownDate = new Date(1523320189511)
	return hash(knownDate.toLocaleString())
}

export async function getBrowserFingerprint(): Promise<string>{
	return hash(await canvasFingerprint() + await jsFingerprint());
}
