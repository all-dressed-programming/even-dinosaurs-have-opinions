import {Router} from 'jsr:@oak/oak/router';

export function createCommentDemoEndpoints(router: Router) {
    router.get('/', (ctx) => {
        ctx.response.body = `<!DOCTYPE html>
    <html>
      <link rel="stylesheet" 
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.6.0/css/all.min.css" 
            integrity="sha512-Kc323vGBEqzTmouAECnVceyQqyqdsSiqLQISBL29aUW4U/M7pSPA/gEUZQqv1cwx4OnYxTxve5UMg5GT6L4JJg==" 
            crossorigin="anonymous" 
            referrerpolicy="no-referrer" />
      <head><title>Hello oak!</title><head>
      <body>
        <h1>Hello oak!</h1>
        <script src="/comments/v1/wui.js" type="module">        
        </script>
        <div id="even-dinosaurs-have-opinions"
             data-page-id="example-1"
             data-api-base-url="/comments"></div>
      </body>
    </html>
  `;
    });
    return router;
}
